import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import cn from 'classnames';
import { LIVES_COUNT } from '../../constants';
import {
  ArrowType,
  ArrowStatus,
  ArrowViews
} from '../../types';
import {
  useAppDispatch,
  useAppSelector,
  selectTasks,
  selectErrors,
  selectSuccesses,
  selectLives,
  pressKey
} from '../../store';


function App() {
  const tasks = useAppSelector(selectTasks);
  const errors = useAppSelector(selectErrors);
  const successes = useAppSelector(selectSuccesses);
  const lives = useAppSelector(selectLives);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (errors === LIVES_COUNT) {
      window.confirm('You lost!');
    }
  }, [ errors ]);

  useEffect(() => {
    if (successes === LIVES_COUNT) {
      window.confirm('You won!');
    }
  }, [ successes ]);

  useEffect(() => {
    window.addEventListener('keydown', event => {
      dispatch(pressKey(event.key as ArrowType));
    });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <section className={'main-container'}>
        <div className="main-container__input">
          <div className="main-container__input-title">Next:</div>
          <div className="main-container__input-values">
            {
              tasks.map((_task, _index) => (
                <div
                  className={
                    cn(
                      'main-container__input-value',
                      {
                        ['main-container__input-value_aligned']: _task.status === ArrowStatus.success,
                        ['main-container__input-value_error']: _task.status === ArrowStatus.error
                      }
                    )
                  }
                  key={_task.id}
                >
                  {' '}
                  {ArrowViews[_task.type]}
                </div>
              ))
            }
          </div>
        </div>
        <div className="main-container__input">
        </div>
        <div className="main-container__input">
          <div className="main-container__input-value main-container__input-value_error">
            {
              Array(lives).fill('❤️').map(_live => _live)
            }
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;