import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store';
import { LIVES_COUNT } from '../../constants';

export const LIVE_SLICE = 'liveSlice';


type LiveSliceInitial = {
  errors: number,
  successes: number
}

const initialState: LiveSliceInitial = {
  errors: 0,
  successes: 0
}

const liveSlice = createSlice({
  name: LIVE_SLICE,
  initialState,
  reducers: {
    addError: (state: LiveSliceInitial) => {
      return {
        ...state,
        errors: state.errors + 1
      }
    },
    addSuccess: (state: LiveSliceInitial) => {
      return {
        ...state,
        successes: state.successes + 1
      }
    }
  }
});

export const selectErrors = (globalState: RootState): number => globalState[LIVE_SLICE].errors;
export const selectSuccesses = (globalState: RootState): number => globalState[LIVE_SLICE].successes;
export const selectLives = (globalState: RootState): number => {
  const _difference = LIVES_COUNT - globalState[LIVE_SLICE].errors;
  return _difference >= 0 ? _difference : 0;
};

export const { addError, addSuccess } = liveSlice.actions;
export const liveSliceReducer = liveSlice.reducer;