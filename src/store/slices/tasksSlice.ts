import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store';
import { Task } from '../../types';

export const TASK_SLICE = 'taskSlice';


type TasksSliceInitial = {
  tasks: Array<Task>,
}

const initialState: TasksSliceInitial = {
  tasks: []
}

const taskSlice = createSlice({
  name: TASK_SLICE,
  initialState,
  reducers: {
    createNewTask: (state: TasksSliceInitial, action: PayloadAction<Task>) => {
      return {
        ...state,
        tasks: [
          ...state.tasks,
          action.payload
        ]
      };
    },
    setTasks: (state: TasksSliceInitial, action: PayloadAction<Array<Task>>) => {
      return {
        ...state.tasks,
        tasks: action.payload
      };
    }
  }
});

export const selectTasks = (globalState: RootState): Array<Task> => globalState[TASK_SLICE].tasks;
export const { createNewTask, setTasks } = taskSlice.actions;
export const taskSliceReducer = taskSlice.reducer;