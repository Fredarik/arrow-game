import {fork, put, select, takeLatest} from 'redux-saga/effects';
import { ArrowStatus, ArrowType, Task } from '../../types';
import { PayloadAction } from '@reduxjs/toolkit'
import { TASK_DELAY } from '../../constants';
import {
  addError,
  addSuccess,
  createNewTask,
  selectTasks,
  setTasks
} from '../slices';
import {
  checkPendingToLastTask,
  checkSuccessArrowType,
  createDefaultTask,
  delay,
  setLastStatus
} from '../../commonFunctions';

export const ARROW_RESOLVE = 'ARROW_RESOLVE';

const arrowResolve = function* ({ payload }: PayloadAction<ArrowType>) {
  const tasks: Array<Task> = yield select(selectTasks);
  if (tasks.length && tasks[tasks.length-1].status === ArrowStatus.pending) {
    const isSuccessArrowType = checkSuccessArrowType(tasks[tasks.length-1], payload);
    yield put(setTasks(setLastStatus(tasks, isSuccessArrowType ? ArrowStatus.success : ArrowStatus.error)));
    if (isSuccessArrowType) {
      yield put(addSuccess());
    } else {
      yield put(addError());
    }
  }
}

const arrowResolveWatcher = function* () {
  yield takeLatest(ARROW_RESOLVE, arrowResolve);
}

const asyncCreateNewTask = function* () {
  yield delay(TASK_DELAY);
  const tasks: Array<Task> = yield select(selectTasks);
  if (tasks.length) {
    const isLastStatusPending = checkPendingToLastTask(tasks);
    if (isLastStatusPending) {
      yield put(addError());
      yield put(setTasks(setLastStatus(tasks, ArrowStatus.error)));
    }
  }
  yield put(createNewTask(createDefaultTask()));
}

const asyncCreateNewTaskInTimeout = function* () {
  while (true) {
    yield asyncCreateNewTask();
  }
}

export const rootSaga = function* () {
  yield fork(asyncCreateNewTaskInTimeout);
  yield fork(arrowResolveWatcher);
}

export const pressKey = (arrowType: ArrowType): PayloadAction<ArrowType> => ({
  type: ARROW_RESOLVE,
  payload: arrowType
})

