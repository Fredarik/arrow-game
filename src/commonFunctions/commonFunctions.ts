import {ArrowStatus, ArrowType, Task} from '../types';

const arrowTypes = [
  ArrowType.ArrowLeft,
  ArrowType.ArrowDown,
  ArrowType.ArrowRight,
  ArrowType.ArrowUp
];

export const getRandomArrowType = ():ArrowType => arrowTypes[Math.floor(Math.random() * 4)];

export const createDefaultTask = ():Task => ({
  type: getRandomArrowType(),
  status: ArrowStatus.pending,
  id: Math.random()
});

export const checkSuccessArrowType = (task: Task, arrowType: ArrowType): boolean => (
  arrowType === task.type
);

export const delay = (tm: number) => new Promise((resolve) => {
  setTimeout(resolve, tm)
});

export const checkPendingToLastTask = (tasks: Array<Task>): boolean => (
  !!tasks.length && tasks[tasks.length - 1].status === ArrowStatus.pending
);

export const setLastStatus = (tasks: Array<Task>, status: ArrowStatus):Array<Task> => {
  const _tasks = [...tasks];
  _tasks[_tasks.length - 1] = {
    ..._tasks[_tasks.length - 1],
    status: status
  }
  return _tasks;
}