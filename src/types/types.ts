export type Key = ArrowType | void;

export enum ArrowType {
  ArrowUp = 'ArrowUp',
  ArrowDown = 'ArrowDown',
  ArrowLeft = 'ArrowLeft',
  ArrowRight = 'ArrowRight'
}

export enum ArrowStatus {
  pending = 'pending',
  success = 'success',
  error = 'error',
}

export type Task  = {
  type: ArrowType;
  status: ArrowStatus,
  id: number
}

export enum ArrowViews {
  ArrowUp = '↑',
  ArrowDown = '↓',
  ArrowLeft = '←',
  ArrowRight = '→',
}